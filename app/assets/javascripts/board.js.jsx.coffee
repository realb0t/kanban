# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $board = $("#wall")
  if $board.length > 0
    React.initializeTouchEvents(true)
    React.render \
      `<div className="board">
        <AddFormComponent />
        <BoardComponent />
      </div>`,
      $board[0]