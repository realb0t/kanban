window.Dispatcher = 
  dragging: null
  tickets: new Tickets()
  handleTicketSubmit: (ticketData) ->
    this.tickets.add(ticketData)
  drag: (ticket) ->
    this.dragging = ticket
  drop: () ->
    this.dragging = null