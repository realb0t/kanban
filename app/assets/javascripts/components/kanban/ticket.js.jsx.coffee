window.TicketComponent = React.createClass(
  getInitialState: ->
    mouseDown: false
    dragging: false
  style: ->
    if @state.dragging
      position: 'absolute'
      left: @state.left
      top: @state.top
      width: @state.width
    else
      {}
  onDragStart: ->
    @props.dragStart(@props.ticket)
  onDragStop: ->
    @props.dragStop()
  eventPosition: (e) ->
    out = {x:0, y:0}
    if (e.type == 'touchstart' || e.type == 'touchmove' || e.type == 'touchend' || e.type == 'touchcancel')
      touch = e.touches[0] || e.changedTouches[0]
      out.x = touch.pageX
      out.y = touch.pageY
    if (e.type == 'mousedown' || e.type == 'mouseup' || e.type == 'mousemove' || e.type == 'mouseover'|| e.type=='mouseout' || e.type=='mouseenter' || e.type=='mouseleave')
      out.x = e.pageX
      out.y = e.pageY
    out
  onMouseDown: (e) ->
    if e.button == 0
      e.stopPropagation()
      @onXDragStart(e)
  onXDragStart: (e) ->
    pos = @eventPosition(e)
    @addEvents()
    pageOffset = @getDOMNode().getBoundingClientRect()
    width = @getDOMNode().clientWidth
    @setState
      mouseDown: true
      width: width
      originX: pos.x
      originY: pos.y
      elementX: pageOffset.left
      elementY: pageOffset.top
  onXDrag: (e) ->
    pos = @eventPosition(e)
    deltaX = pos.x - @state.originX
    deltaY = pos.y - @state.originY
    distance = Math.abs(deltaX) + Math.abs(deltaY)
    if !@state.dragging and distance > 3
      @setState(dragging: true)
      @onDragStart()
    if @state.dragging
      @setState
        left: @state.elementX + deltaX + document.body.scrollLeft
        top: @state.elementY + deltaY + document.body.scrollTop
  onXDragEnd: (e) ->
    pos = @eventPosition(e)
    @removeEvents()
    if @state.dragging
      @onDragStop()
      @setState(dragging: false) if @isMounted()
  onTouchStart: (e) ->
    e.preventDefault()
    @onXDragStart(e)
  onTouchEnd: (e) ->
    # Эмуляция события mouseup
    pos = @eventPosition(e)
    ev = document.createEvent("MouseEvent");
    el = document.elementFromPoint(pos.x, pos.y);
    ev.initMouseEvent("mouseup", true, true);
    el.dispatchEvent(ev);
  addEvents: ->
    document.addEventListener 'mousemove', @onXDrag
    document.addEventListener 'mouseup', @onXDragEnd
    document.addEventListener 'touchmove', @onXDrag
    document.addEventListener 'touchend', @onTouchEnd
  removeEvents: ->
    document.removeEventListener 'mousemove', @onXDrag
    document.removeEventListener 'mouseup', @onXDragEnd
    document.removeEventListener 'touchmove', @onXDrag
    document.removeEventListener 'touchend', @onTouchEnd
  render: ->
    classes = "dnd-draggable ticket" 
    classes += " dragging" if @state.dragging
    styles = @style()
    `<div className={classes} style={styles} onMouseDown={this.onMouseDown} onTouchStart={this.onTouchStart} ref="ticket">
      <div className="ticket__title">{this.props.title}</div>
      <div className="ticket__desc">{this.props.description}</div>
    </div>`
)