window.BoardComponent = React.createClass(
  getInitialState: -> dragging: false
  onDragStart: () -> @setState(dragging: true)
  onDragStop: () -> @setState(dragging: false)
  render: ->
    classes = 'board__states'
    classes += ' board_drag_dragging' if @state.dragging
    `<div className={classes}>
      <StateComponent 
        onDragStart={this.onDragStart}
        onDragStop={this.onDragStop}
        state="todo"></StateComponent>
      <StateComponent 
        onDragStart={this.onDragStart}
        onDragStop={this.onDragStop}
        state="doing"></StateComponent>
      <StateComponent 
        onDragStart={this.onDragStart}
        onDragStop={this.onDragStop}
        state="done"></StateComponent>
    </div>`
)