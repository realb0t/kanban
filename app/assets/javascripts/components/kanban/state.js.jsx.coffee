window.StateComponent = React.createClass(
  getInitialState: ->
    hover: false
  tickets: ->
    Dispatcher.tickets.where(state: @props.state)
  getInitialState: ->
    { count: 0 }
  onChangeTickets: ->
    @setState(count: @tickets().length)
  componentDidMount: ->
    Dispatcher.tickets.on 'add', @onChangeTickets
    Dispatcher.tickets.on 'change:state', @onChangeTickets
  onMouseEnter: (e) -> 
    @setState hover: true
  onMouseLeave: (e) ->
    @setState hover: false
  onDragStart: (ticket) ->
    @props.onDragStart()
    Dispatcher.drag(ticket)
  onDragStop: () ->
    @props.onDragStop()
    Dispatcher.drop()
  onMouseUp: (e, params) ->
    # Для эмуляции mouseUp с touchEnd события
    syntetic = e.pageY == 0 && e.pageX == 0
    if Dispatcher.dragging && (@state.hover || syntetic)
      Dispatcher.dragging.applyState(@props.state)
  render: ->
    classes = 'state state_state_' + @props.state
    onDragStart = @onDragStart
    onDragStop = @onDragStop
    tickets = @tickets().map (ticket) ->
      `<TicketComponent
        key={ticket.cid}
        ticket={ticket}
        title={ticket.get('title')} 
        description={ticket.get('description')}
        dragStart={onDragStart}
        dragStop={onDragStop} />`

    `<div className={classes} 
      onMouseUp={this.onMouseUp}
      onMouseEnter={this.onMouseEnter} 
      onMouseLeave={this.onMouseLeave}>
      <h1 className="state__title">
        <span className="state__name">{this.props.state}</span>
        <span className="state__count">({this.state.count})</span>
      </h1>
      <div className="state__tickets">
        {tickets}
      </div>
    </div>`
)