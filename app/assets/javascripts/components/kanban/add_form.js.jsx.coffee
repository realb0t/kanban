window.AddFormComponent = React.createClass(
  componentDidMount: ->
    @titleField = this.refs.title.getDOMNode()
    @descField  = this.refs.description.getDOMNode()
    @modal      = this.refs.modal.getDOMNode()
  onTicketSubmit: (e) ->
    e.preventDefault()
    title = @titleField.value.trim()
    desc  = @descField.value.trim()
    return if (!title || !desc)
    Dispatcher.handleTicketSubmit \
      title: title,
      description: desc
    $(@modal).modal('hide')
    @titleField.value = null
    @descField.value = null
  render: ->
    `<div className="board__add-form">
      <div className="board__add-form-window modal fade" ref="modal" id="addForm" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <form role="form" onClick={this.onTicketSubmit}>
              <div className="modal-body">
                <div className="form-group">
                  <label htmlFor="ticketTitle">Title</label>
                  <input className="form-control" type="text" id="ticketTitle" ref="title" placeholder="Enter title"/>
                </div>
                <div className="form-group">
                  <label htmlFor="ticketDescription">Description</label>
                  <input className="form-control" type="text" id="ticketDescription" ref="description" placeholder="Enter description"/>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" className="btn btn-primary">Create ticket</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="board__add-form-button" data-toggle="modal" data-target="#addForm">
        <span className="glyphicon glyphicon-plus" aria-hidden="true" />
        <span className="board__add-form-button__title">Добавить тикет</span>
      </div>
    </div>`
)