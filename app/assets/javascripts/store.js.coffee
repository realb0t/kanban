class Ticket extends Backbone.Model
  defaults:
    title: null,
    description: null,
    state: 'todo'
  avalibleStates: [ 'todo', 'doing', 'done' ]
  applyState: (nextState) ->
    return unless _(@avalibleStates).include(nextState)
    currentState = @get('state')
    return if currentState == nextState
    @set('state', nextState) if @avalibleTransition(currentState, nextState)
  avalibleTransition: (current, next) ->
    ((current == 'todo' || current == 'done') && next == 'doing') ||
    (current == 'doing' && (next == 'todo' || next == 'done'))

class window.Tickets extends Backbone.Collection
  model: Ticket