# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Kanban::Application.config.secret_key_base = '4c98299d39a4d7c460bcd97c3c95da1ac15446690ba21934ef50072c0a9c13152bcbd5ca075062a5e440b69d1603b20d86ddb8515134068e09963bb6b77b8bc2'
